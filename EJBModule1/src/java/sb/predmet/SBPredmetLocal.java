/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sb.predmet;

import domen.Predmet;
import domen.Profesor;
import domen.Profesorpredmet;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Katarina
 */
@Local
public interface SBPredmetLocal {

    List<Predmet> vratiSvePredmete();

    List<Profesorpredmet> vratiPredmeteZaProfesora(Profesor profesor);

    Predmet vratiPredmetZaID(int predmetID);
    
}
