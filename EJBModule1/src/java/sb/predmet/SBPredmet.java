/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sb.predmet;

import domen.Predmet;
import domen.Profesor;
import domen.Profesorpredmet;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Katarina
 */
@Stateless
public class SBPredmet implements SBPredmetLocal {
 @PersistenceContext(unitName = "EJBModule1PU")
    private EntityManager em;
    
    
    @Override
    public List<Predmet> vratiSvePredmete() {
        List<Predmet> lista = em.createQuery("Select p from Predmet p").getResultList();
        return lista;
    }
    
   

    @Override
    public List<Profesorpredmet> vratiPredmeteZaProfesora(Profesor profesor) {
        
        List<Profesorpredmet> lista = em.createQuery("select p from Profesorpredmet p where p.idProfesora.idProfesora=:profID").setParameter("profID", profesor.getIdProfesora()).getResultList();
        return lista;
    }

    @Override
    public Predmet vratiPredmetZaID(int predmetID) {
       Predmet p = em.find(Predmet.class, predmetID);
       return p;
    }
    
    
}
