/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sb.student;

import domen.Student;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Katarina
 */
@Stateless
public class SBStudent implements SBStudentLocal{
@PersistenceContext(unitName = "EJBModule1PU")
    private EntityManager em;
   
   @Override
    public Student vratiStudenta(Student student) throws Exception{
         Student studentDB = null;
        try{
        studentDB = (Student) em.createQuery("SELECT s FROM Student s WHERE s.emailStudenta=:email AND s.sifraStudenta=:sifra").setParameter("email", student.getEmailStudenta()).setParameter("sifra",student.getSifraStudenta()).getSingleResult();
        }catch(NoResultException nre){
            nre.getMessage();
            throw new Exception("Ne postoji korisnik sa unetim podacima!");
        }
        return studentDB;
    }

    @Override
    public List<Student> vratiStudente(String ime) {
       List<Student> lista = em.createNativeQuery("Select * from student where imeStudenta like '%"+ime+"%'").getResultList();
       return lista;
    }

    @Override
    public List<Student> vratiSveStudente() {
        List<Student> lista = em.createQuery("select s from Student s").getResultList();
       return lista;
    }

    @Override
    public Student vratiStudentaZaID(int id) {
       Student s = em.find(Student.class, id);
       return s;
    }

    
}
