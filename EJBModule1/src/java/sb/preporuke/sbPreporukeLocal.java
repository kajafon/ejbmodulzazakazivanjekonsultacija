/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sb.preporuke;


import domen.Student;
import java.util.HashMap;
import javax.ejb.Local;

/**
 *
 * @author Kaja
 */
@Local
public interface sbPreporukeLocal {

  public  HashMap<String,Object> vratiSmeroveZaMaster(Student listaOcena);
    
}
