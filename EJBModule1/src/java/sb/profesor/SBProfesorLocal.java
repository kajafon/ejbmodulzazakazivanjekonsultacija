/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sb.profesor;

import domen.Profesor;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Katarina
 */
@Local
public interface SBProfesorLocal {

    

    List<Profesor> vratiSveProfesore();

    Profesor vratiProfesoraZaId(int idProfesora);

    Profesor vratiProfesora(Profesor profesor)throws Exception;
    
}
