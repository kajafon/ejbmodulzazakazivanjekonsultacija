/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sb.profesor;

import domen.Profesor;
import domen.Student;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Katarina
 */
@Stateless
public class SBProfesor implements SBProfesorLocal {
@PersistenceContext(unitName = "EJBModule1PU")
    private EntityManager em;
    
    @Override
    public List<Profesor> vratiSveProfesore() {
        List<Profesor> lista = em.createQuery("select p from Profesor p").getResultList();
        return lista;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public Profesor vratiProfesoraZaId(int idProfesora) {
       Profesor profesor =em.find(Profesor.class, idProfesora);
       return profesor;
    }  

    @Override
    public Profesor vratiProfesora(Profesor profesor) throws Exception{
        Profesor profesorDB = null;
        try{
        profesorDB = (Profesor) em.createQuery("SELECT p FROM Profesor p WHERE p.emailProfesora=:email AND p.sifraProfesora=:sifra").setParameter("email", profesor.getEmailProfesora()).setParameter("sifra",profesor.getSifraProfesora()).getSingleResult();
        }catch(NoResultException nre){
            nre.getMessage();
            throw new Exception("Ne postoji korisnik sa unetim podacima!");
        }
        return profesorDB;
    }
      
    
}
