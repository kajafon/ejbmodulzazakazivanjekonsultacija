/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sb.konsultacije;

import domen.Konsultacija;
import domen.Profesor;
import domen.Student;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Katarina
 */
@Local
public interface SBKonsultacijeLocal {

    List<Konsultacija> vratiKonsultacije();

    List<Konsultacija> vratiKonsultacijeZaDatumIProfesora(Date datum, Profesor profesor);

    void zakaziKonsultaciju(Konsultacija konsultacija);

    void otkaziKonsultaciju(Konsultacija konsultacija);

    void dodajKonsultaciju();
    
    public void dodajKonsultacije(String datum, String vremePocetka, String vremeZavrsetka, int period, Profesor profesor);

    void obrisiKonsultaciju(Konsultacija konsultacija);

    void dodajKomentarProfesora(Konsultacija k);

    List<Date> vratiDatumeKonsultacijaZaProfesora(Profesor profesor);

    List<Konsultacija> vratiKonsultacijeZaProfesoraIStudenta(Student student, Profesor profesor);
}
