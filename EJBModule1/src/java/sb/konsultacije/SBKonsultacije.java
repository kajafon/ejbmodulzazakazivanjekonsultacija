/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sb.konsultacije;

import domen.Konsultacija;
import domen.Profesor;
import domen.Student;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mail.Mail;

/**
 *
 * @author Katarina
 */
@Stateless
public class SBKonsultacije implements SBKonsultacijeLocal {

    @PersistenceContext(unitName = "EJBModule1PU")
    private EntityManager em;

    @Override
    public List<Konsultacija> vratiKonsultacije() {
        List<Konsultacija> lista = em.createQuery("select k from Konsultacija k").getResultList();

        return lista;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public List<Konsultacija> vratiKonsultacijeZaDatumIProfesora(Date datum, Profesor profesor) {
       // if(datum==null  profesor==null){
   // return new ArrayList<Konsultacija>();
  //  }
        List<Konsultacija> lista = em.createQuery("select k from Konsultacija k where k.konsultacijaPK.datum =:datum and k.konsultacijaPK.profesor=:profesor ORDER BY k.konsultacijaPK.vremePocetka").setParameter("datum", datum).setParameter("profesor", profesor.getIdProfesora()).getResultList();
        return lista;
    }

    @Override
    public void zakaziKonsultaciju(Konsultacija konsultacija) {

        Konsultacija k = em.find(Konsultacija.class, konsultacija.getKonsultacijaPK());
       // Konsultacija k = (Konsultacija) em.createQuery("select k from Konsultacija k where k.idKonsultacije =:idk").setParameter("idk", konsultacija.getIdKonsultacije()).getSingleResult();
        //  List<Konsultacija> k =  em.createQuery("select k from Konsultacija k where k.konsultacijaPK.datum =:datum and k.konsultacijaPK.profesor=:prof and k.konsultacijaPK.vremePocetka=:vremeP and k.konsultacijaPK.vremeZavrsetka=:vremeK").setParameter("datum", konsultacija.getKonsultacijaPK().getDatum()).setParameter("prof", konsultacija.getKonsultacijaPK().getProfesor()).setParameter("vremeP", konsultacija.getKonsultacijaPK().getVremePocetka()).setParameter("vremeK", konsultacija.getKonsultacijaPK().getVremeZavrsetka()).getResultList();
        //  List<Konsultacija> lista = em.createQuery("select k from Konsultacija k where k.konsultacijaPK.datum =:datum and k.konsultacijaPK.profesor=:profesor").setParameter("datum", konsultacija.getKonsultacijaPK().getDatum()).setParameter("profesor", konsultacija.getKonsultacijaPK().getProfesor()).getResultList();

//List<Konsultacija> lista = em.createQuery("select k from Konsultacija k").getResultList();
        if (k == null) {
            em.persist(konsultacija);
        } else {

            em.merge(konsultacija);
        }
        Profesor p= konsultacija.getProfesor1();
        String [] primalac = {p.getEmailProfesora().trim()};
        Mail.sendFromGMail("bgdfon", "bgdfonbgdfon",primalac , "Zakazana konsultacija", "Postovani "+p.getImeProfesora()+" "+p.getPrezimeProfesora()+",\n\nObavestavamo Vas da je student "+konsultacija.getStudent().getImeStudenta()+" "+konsultacija.getStudent().getPrezimeStudenta()+" zakazao termin konsultacije.\n\nS poštovanjem,\nFakultet organizacionih nauka");
       // em.createNativeQuery("update konsultacije set komentarStudenta="+konsultacija.getKomentarStudenta()+", student = "+konsultacija.getStudent()+"where idKonsultacije="+konsultacija.getIdKonsultacije());
        // em.flush();

    }

    @Override
    public void otkaziKonsultaciju(Konsultacija konsultacija) {
    
     Konsultacija k = em.find(Konsultacija.class, konsultacija.getKonsultacijaPK());
     Student s = konsultacija.getStudent();
       konsultacija.setKomentarStudenta(null);
       konsultacija.setPredmet(null);
       konsultacija.setStudent(null);
       
        if (k == null) {
            em.persist(konsultacija);
        } else {
            em.merge(konsultacija);
        }
        Profesor p= konsultacija.getProfesor1();
        String [] primalac = {p.getEmailProfesora().trim()};
        Mail.sendFromGMail("bgdfon", "bgdfonbgdfon",primalac , "Otkazana konsultacija", "Postovani "+p.getImeProfesora()+" "+p.getPrezimeProfesora()+",\n\nObavestavamo Vas da je student "+s.getImeStudenta()+" "+s.getPrezimeStudenta()+" otkazao termin konsultacije.\n\nS poštovanjem,\nFakultet organizacionih nauka");
      
    }  

    
    
    @Override
    public void dodajKonsultaciju() {
        em.createNativeQuery("INSERT INTO konsultacija( datum, vremePocetka, vremeZavrsetka, student, profesor ) VALUES ('21.01.2014',  '16:00',  '16:45', 1, 2)").executeUpdate();
    }
    
    @Override
    public void dodajKonsultacije(String datum, String vremePocetka, String vremeZavrsetka, int period, Profesor profesor){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            Date pocetakTermina = format.parse(vremePocetka);
            Date krajKonsultacija = format.parse(vremeZavrsetka);
            
            Date krajTermina=dodajMinute(pocetakTermina, period);
            
            while(krajKonsultacija.after(krajTermina)  || krajKonsultacija.equals(krajTermina)){
               Calendar calendar= Calendar.getInstance();
               calendar.setTime(pocetakTermina);
               String formatiranPocetakTermina = calendar.get(Calendar.HOUR_OF_DAY) + ":" + (calendar.get(Calendar.MINUTE));
                
               Calendar calendar1= Calendar.getInstance();
               
               calendar1.setTime(krajTermina);
               
               String formatiranKrajTermina = calendar1.get(Calendar.HOUR_OF_DAY) + ":" + (calendar1.get(Calendar.MINUTE));
           // List<Konsultacija> lista = em.createQuery("SELECT k from Konsultacija k WHERE k.konsultacijaPK.datum=:datum and ((k.konsultacijaPK.vremePocetka<:vremeP AND vremeZavrsetka >: vremeP) or (k.konsultacijaPK.vremePocetka<:vremeZ AND vremeZavrsetka >: vremeZ))" , Konsultacija.class).setParameter("datum", datum).setParameter("vremeP", formatiranPocetakTermina).setParameter("vremeZ", formatiranKrajTermina).getResultList();
            List<Konsultacija> lista = em.createNativeQuery("SELECT * FROM  konsultacija WHERE datum = DATE '"+datum+"' AND (" +
"(" +
"vremePocetka <= TIME '"+formatiranPocetakTermina+"'" +
"AND vremeZavrsetka > TIME '"+formatiranPocetakTermina+"'" +
")" +
"OR (" +
"vremePocetka < TIME '"+formatiranKrajTermina+"'" +
"AND vremeZavrsetka >= TIME '"+formatiranKrajTermina+"'" +
")" +
")").getResultList();
               if(lista==null || lista.isEmpty()){
               em.createNativeQuery("INSERT INTO konsultacija( datum, vremePocetka, vremeZavrsetka, profesor ) VALUES "
                    + "('"+datum+"', '"+formatiranPocetakTermina+"', '"+formatiranKrajTermina+"', "+profesor.getIdProfesora()+")").executeUpdate();
            }
               pocetakTermina=krajTermina;
            krajTermina=dodajMinute(krajTermina, period);
            
            }
            
        } catch (ParseException ex) {
            Logger.getLogger(SBKonsultacije.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Date dodajMinute(Date pocetni, int period){
     Calendar cal = Calendar.getInstance();
            cal.setTime(pocetni);
            cal.add(Calendar.MINUTE, period);
            Date kraj=cal.getTime();
            return kraj;
    }

    @Override
    public void obrisiKonsultaciju(Konsultacija konsultacija) {
         Konsultacija k = em.find(Konsultacija.class, konsultacija.getKonsultacijaPK());
        if (k != null) {    
            em.remove(k);
            if(konsultacija.getStudent()!=null){
        String [] primalac = {konsultacija.getStudent().getEmailStudenta().trim()};
        Mail.sendFromGMail("bgdfon", "bgdfonbgdfon",primalac , "Otkazana konsultacija", "Postovani "+konsultacija.getStudent().getImeStudenta()+" "+konsultacija.getStudent().getPrezimeStudenta()+",\n\nObavestavamo Vas da je profesor "+konsultacija.getProfesor1().getImeProfesora()+" "+konsultacija.getProfesor1().getPrezimeProfesora()+" otkazao termin konsultacije.\n\nS poštovanjem,\nFakultet organizacionih nauka");
            }
        } 
        
    }

    @Override
    public void dodajKomentarProfesora(Konsultacija k) {
        
    //    Konsultacija kon = em.find(Konsultacija.class, k.getKonsultacijaPK());
    
     //  k.setKomentarStudenta(null);
     //  konsultacija.setPredmet(null);
      // konsultacija.setStudent(null);
       
      //  if (k == null) {
      //      em.persist(konsultacija);
      //  } else {
            em.merge(k);
       // }
    }

    @Override
    public List<Date> vratiDatumeKonsultacijaZaProfesora(Profesor profesor) {
        List<Date> lista = em.createNativeQuery("Select distinct datum from konsultacija where datum>now() and profesor = "+profesor.getIdProfesora()).getResultList();
        return lista;
    }

    @Override
    public List<Konsultacija> vratiKonsultacijeZaProfesoraIStudenta(Student student, Profesor profesor) {
        List<Konsultacija> lista = em.createQuery("select k from Konsultacija k where k.student.idStudent =:student and k.konsultacijaPK.profesor=:profesor ORDER BY k.konsultacijaPK.datum,k.konsultacijaPK.vremePocetka").setParameter("student", student.getIdStudent()).setParameter("profesor", profesor.getIdProfesora()).getResultList();
        return lista;
       
    }
    
    
}
