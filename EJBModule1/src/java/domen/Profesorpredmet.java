/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Katarina
 */
@Entity
@Table(name = "profesorpredmet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profesorpredmet.findAll", query = "SELECT p FROM Profesorpredmet p"),
    @NamedQuery(name = "Profesorpredmet.findByIdVezeProfesorPredmet", query = "SELECT p FROM Profesorpredmet p WHERE p.idVezeProfesorPredmet = :idVezeProfesorPredmet")})
public class Profesorpredmet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idVezeProfesorPredmet")
    private Integer idVezeProfesorPredmet;
    @JoinColumn(name = "idProfesora", referencedColumnName = "idProfesora")
    @ManyToOne(optional = false)
    private Profesor idProfesora;
    @JoinColumn(name = "idPredmeta", referencedColumnName = "idPredmeta")
    @ManyToOne(optional = false)
    private Predmet idPredmeta;

    public Profesorpredmet() {
    }

    public Profesorpredmet(Integer idVezeProfesorPredmet) {
        this.idVezeProfesorPredmet = idVezeProfesorPredmet;
    }

    public Integer getIdVezeProfesorPredmet() {
        return idVezeProfesorPredmet;
    }

    public void setIdVezeProfesorPredmet(Integer idVezeProfesorPredmet) {
        this.idVezeProfesorPredmet = idVezeProfesorPredmet;
    }

    public Profesor getIdProfesora() {
        return idProfesora;
    }

    public void setIdProfesora(Profesor idProfesora) {
        this.idProfesora = idProfesora;
    }

    public Predmet getIdPredmeta() {
        return idPredmeta;
    }

    public void setIdPredmeta(Predmet idPredmeta) {
        this.idPredmeta = idPredmeta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVezeProfesorPredmet != null ? idVezeProfesorPredmet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesorpredmet)) {
            return false;
        }
        Profesorpredmet other = (Profesorpredmet) object;
        if ((this.idVezeProfesorPredmet == null && other.idVezeProfesorPredmet != null) || (this.idVezeProfesorPredmet != null && !this.idVezeProfesorPredmet.equals(other.idVezeProfesorPredmet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Profesorpredmet[ idVezeProfesorPredmet=" + idVezeProfesorPredmet + " ]";
    }
    
}
