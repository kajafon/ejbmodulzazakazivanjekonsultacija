/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Katarina
 */
@Embeddable
public class KonsultacijaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "datum")
    @Temporal(TemporalType.DATE)
    private Date datum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vremePocetka")
    @Temporal(TemporalType.TIME)
    private Date vremePocetka;
    @Basic(optional = false)
    @NotNull
    @Column(name = "profesor")
    private int profesor;

    public KonsultacijaPK() {
    }

    public KonsultacijaPK(Date datum, Date vremePocetka, int profesor) {
        this.datum = datum;
        this.vremePocetka = vremePocetka;
        this.profesor = profesor;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Date getVremePocetka() {
        return vremePocetka;
    }

    public void setVremePocetka(Date vremePocetka) {
        this.vremePocetka = vremePocetka;
    }

    public int getProfesor() {
        return profesor;
    }

    public void setProfesor(int profesor) {
        this.profesor = profesor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (datum != null ? datum.hashCode() : 0);
        hash += (vremePocetka != null ? vremePocetka.hashCode() : 0);
        hash += (int) profesor;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KonsultacijaPK)) {
            return false;
        }
        KonsultacijaPK other = (KonsultacijaPK) object;
        if ((this.datum == null && other.datum != null) || (this.datum != null && !this.datum.equals(other.datum))) {
            return false;
        }
        if ((this.vremePocetka == null && other.vremePocetka != null) || (this.vremePocetka != null && !this.vremePocetka.equals(other.vremePocetka))) {
            return false;
        }
        if (this.profesor != other.profesor) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.KonsultacijaPK[ datum=" + datum + ", vremePocetka=" + vremePocetka + ", profesor=" + profesor + " ]";
    }
    
}
