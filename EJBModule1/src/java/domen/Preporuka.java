/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;

/**
 *
 * @author Kaja
 */
public class Preporuka implements Serializable{
    int brojSmera;
    String opisSmera;
    double prosekOcena;

    public int getBrojSmera() {
        return brojSmera;
    }

    public void setBrojSmera(int brojSmera) {
        this.brojSmera = brojSmera;
    }

    public String getOpisSmera() {
        return opisSmera;
    }

    public void setOpisSmera(String opisSmera) {
        this.opisSmera = opisSmera;
    }

    public double getProsekOcena() {
        return prosekOcena;
    }

    public void setProsekOcena(double prosekOcena) {
        this.prosekOcena = prosekOcena;
    }

    public Preporuka() {
    }
    
    
}
