/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Katarina
 */
@Entity
@Table(name = "konsultacija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Konsultacija.findAll", query = "SELECT k FROM Konsultacija k"),
    @NamedQuery(name = "Konsultacija.findByIdKonsultacije", query = "SELECT k FROM Konsultacija k WHERE k.idKonsultacije = :idKonsultacije"),
    @NamedQuery(name = "Konsultacija.findByDatum", query = "SELECT k FROM Konsultacija k WHERE k.konsultacijaPK.datum = :datum"),
    @NamedQuery(name = "Konsultacija.findByVremePocetka", query = "SELECT k FROM Konsultacija k WHERE k.konsultacijaPK.vremePocetka = :vremePocetka"),
    @NamedQuery(name = "Konsultacija.findByVremeZavrsetka", query = "SELECT k FROM Konsultacija k WHERE k.vremeZavrsetka = :vremeZavrsetka"),
    @NamedQuery(name = "Konsultacija.findByProfesor", query = "SELECT k FROM Konsultacija k WHERE k.konsultacijaPK.profesor = :profesor"),
    @NamedQuery(name = "Konsultacija.findByKomentarProfesora", query = "SELECT k FROM Konsultacija k WHERE k.komentarProfesora = :komentarProfesora"),
    @NamedQuery(name = "Konsultacija.findByKomentarStudenta", query = "SELECT k FROM Konsultacija k WHERE k.komentarStudenta = :komentarStudenta")})
public class Konsultacija implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected KonsultacijaPK konsultacijaPK;
    @Basic(optional = false)
    @Column(name = "idKonsultacije")
    private int idKonsultacije;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vremeZavrsetka")
    @Temporal(TemporalType.TIME)
    private Date vremeZavrsetka;
    @Size(max = 45)
    @Column(name = "komentarProfesora")
    private String komentarProfesora;
    @Size(max = 45)
    @Column(name = "komentarStudenta")
    private String komentarStudenta;
    @JoinColumn(name = "profesor", referencedColumnName = "idProfesora", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Profesor profesor1;
    @JoinColumn(name = "student", referencedColumnName = "idStudent")
    @ManyToOne
    private Student student;
    @JoinColumn(name = "predmet", referencedColumnName = "idPredmeta")
    @ManyToOne
    private Predmet predmet;

    public Konsultacija() {
    }

    public Konsultacija(KonsultacijaPK konsultacijaPK) {
        this.konsultacijaPK = konsultacijaPK;
    }

    public Konsultacija(KonsultacijaPK konsultacijaPK, int idKonsultacije, Date vremeZavrsetka) {
        this.konsultacijaPK = konsultacijaPK;
        this.idKonsultacije = idKonsultacije;
        this.vremeZavrsetka = vremeZavrsetka;
    }

    public Konsultacija(Date datum, Date vremePocetka, int profesor) {
        this.konsultacijaPK = new KonsultacijaPK(datum, vremePocetka, profesor);
    }

    public KonsultacijaPK getKonsultacijaPK() {
        return konsultacijaPK;
    }

    public void setKonsultacijaPK(KonsultacijaPK konsultacijaPK) {
        this.konsultacijaPK = konsultacijaPK;
    }

    public int getIdKonsultacije() {
        return idKonsultacije;
    }

    public void setIdKonsultacije(int idKonsultacije) {
        this.idKonsultacije = idKonsultacije;
    }

    public Date getVremeZavrsetka() {
        return vremeZavrsetka;
    }

    public void setVremeZavrsetka(Date vremeZavrsetka) {
        this.vremeZavrsetka = vremeZavrsetka;
    }

    public String getKomentarProfesora() {
        return komentarProfesora;
    }

    public void setKomentarProfesora(String komentarProfesora) {
        this.komentarProfesora = komentarProfesora;
    }

    public String getKomentarStudenta() {
        return komentarStudenta;
    }

    public void setKomentarStudenta(String komentarStudenta) {
        this.komentarStudenta = komentarStudenta;
    }

    public Profesor getProfesor1() {
        return profesor1;
    }

    public void setProfesor1(Profesor profesor1) {
        this.profesor1 = profesor1;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Predmet getPredmet() {
        return predmet;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (konsultacijaPK != null ? konsultacijaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Konsultacija)) {
            return false;
        }
        Konsultacija other = (Konsultacija) object;
        if ((this.konsultacijaPK == null && other.konsultacijaPK != null) || (this.konsultacijaPK != null && !this.konsultacijaPK.equals(other.konsultacijaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Konsultacija[ konsultacijaPK=" + konsultacijaPK + " ]";
    }
    
}
