/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Katarina
 */
@Entity
@Table(name = "student")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s"),
    @NamedQuery(name = "Student.findByIdStudent", query = "SELECT s FROM Student s WHERE s.idStudent = :idStudent"),
    @NamedQuery(name = "Student.findByBrojIndeksa", query = "SELECT s FROM Student s WHERE s.brojIndeksa = :brojIndeksa"),
    @NamedQuery(name = "Student.findByImeStudenta", query = "SELECT s FROM Student s WHERE s.imeStudenta = :imeStudenta"),
    @NamedQuery(name = "Student.findByPrezimeStudenta", query = "SELECT s FROM Student s WHERE s.prezimeStudenta = :prezimeStudenta"),
    @NamedQuery(name = "Student.findByEmailStudenta", query = "SELECT s FROM Student s WHERE s.emailStudenta = :emailStudenta"),
    @NamedQuery(name = "Student.findBySifraStudenta", query = "SELECT s FROM Student s WHERE s.sifraStudenta = :sifraStudenta")})
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idStudent")
    private Integer idStudent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "brojIndeksa")
    private String brojIndeksa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "imeStudenta")
    private String imeStudenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "prezimeStudenta")
    private String prezimeStudenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "emailStudenta")
    private String emailStudenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "sifraStudenta")
    private String sifraStudenta;
    @OneToMany(mappedBy = "student")
    private List<Konsultacija> konsultacijaList;

    public Student() {
    }

    public Student(Integer idStudent) {
        this.idStudent = idStudent;
    }

    public Student(Integer idStudent, String brojIndeksa, String imeStudenta, String prezimeStudenta, String emailStudenta, String sifraStudenta) {
        this.idStudent = idStudent;
        this.brojIndeksa = brojIndeksa;
        this.imeStudenta = imeStudenta;
        this.prezimeStudenta = prezimeStudenta;
        this.emailStudenta = emailStudenta;
        this.sifraStudenta = sifraStudenta;
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public String getImeStudenta() {
        return imeStudenta;
    }

    public void setImeStudenta(String imeStudenta) {
        this.imeStudenta = imeStudenta;
    }

    public String getPrezimeStudenta() {
        return prezimeStudenta;
    }

    public void setPrezimeStudenta(String prezimeStudenta) {
        this.prezimeStudenta = prezimeStudenta;
    }

    public String getEmailStudenta() {
        return emailStudenta;
    }

    public void setEmailStudenta(String emailStudenta) {
        this.emailStudenta = emailStudenta;
    }

    public String getSifraStudenta() {
        return sifraStudenta;
    }

    public void setSifraStudenta(String sifraStudenta) {
        this.sifraStudenta = sifraStudenta;
    }

    @XmlTransient
    public List<Konsultacija> getKonsultacijaList() {
        return konsultacijaList;
    }

    public void setKonsultacijaList(List<Konsultacija> konsultacijaList) {
        this.konsultacijaList = konsultacijaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStudent != null ? idStudent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.idStudent == null && other.idStudent != null) || (this.idStudent != null && !this.idStudent.equals(other.idStudent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Student[ idStudent=" + idStudent + " ]";
    }
    
}
