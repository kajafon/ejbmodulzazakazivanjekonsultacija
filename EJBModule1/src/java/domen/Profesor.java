/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Katarina
 */
@Entity
@Table(name = "profesor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profesor.findAll", query = "SELECT p FROM Profesor p"),
    @NamedQuery(name = "Profesor.findByIdProfesora", query = "SELECT p FROM Profesor p WHERE p.idProfesora = :idProfesora"),
    @NamedQuery(name = "Profesor.findByTitula", query = "SELECT p FROM Profesor p WHERE p.titula = :titula"),
    @NamedQuery(name = "Profesor.findByImeProfesora", query = "SELECT p FROM Profesor p WHERE p.imeProfesora = :imeProfesora"),
    @NamedQuery(name = "Profesor.findByPrezimeProfesora", query = "SELECT p FROM Profesor p WHERE p.prezimeProfesora = :prezimeProfesora"),
    @NamedQuery(name = "Profesor.findByEmailProfesora", query = "SELECT p FROM Profesor p WHERE p.emailProfesora = :emailProfesora"),
    @NamedQuery(name = "Profesor.findBySifraProfesora", query = "SELECT p FROM Profesor p WHERE p.sifraProfesora = :sifraProfesora")})
public class Profesor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProfesora")
    private Integer idProfesora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "titula")
    private String titula;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "imeProfesora")
    private String imeProfesora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "prezimeProfesora")
    private String prezimeProfesora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "emailProfesora")
    private String emailProfesora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "sifraProfesora")
    private String sifraProfesora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProfesora")
    private List<Profesorpredmet> profesorpredmetList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profesor1")
    private List<Konsultacija> konsultacijaList;

    public Profesor() {
    }

    public Profesor(Integer idProfesora) {
        this.idProfesora = idProfesora;
    }

    public Profesor(Integer idProfesora, String titula, String imeProfesora, String prezimeProfesora, String emailProfesora, String sifraProfesora) {
        this.idProfesora = idProfesora;
        this.titula = titula;
        this.imeProfesora = imeProfesora;
        this.prezimeProfesora = prezimeProfesora;
        this.emailProfesora = emailProfesora;
        this.sifraProfesora = sifraProfesora;
    }

    public Integer getIdProfesora() {
        return idProfesora;
    }

    public void setIdProfesora(Integer idProfesora) {
        this.idProfesora = idProfesora;
    }

    public String getTitula() {
        return titula;
    }

    public void setTitula(String titula) {
        this.titula = titula;
    }

    public String getImeProfesora() {
        return imeProfesora;
    }

    public void setImeProfesora(String imeProfesora) {
        this.imeProfesora = imeProfesora;
    }

    public String getPrezimeProfesora() {
        return prezimeProfesora;
    }

    public void setPrezimeProfesora(String prezimeProfesora) {
        this.prezimeProfesora = prezimeProfesora;
    }

    public String getEmailProfesora() {
        return emailProfesora;
    }

    public void setEmailProfesora(String emailProfesora) {
        this.emailProfesora = emailProfesora;
    }

    public String getSifraProfesora() {
        return sifraProfesora;
    }

    public void setSifraProfesora(String sifraProfesora) {
        this.sifraProfesora = sifraProfesora;
    }

    @XmlTransient
    public List<Profesorpredmet> getProfesorpredmetList() {
        return profesorpredmetList;
    }

    public void setProfesorpredmetList(List<Profesorpredmet> profesorpredmetList) {
        this.profesorpredmetList = profesorpredmetList;
    }

    @XmlTransient
    public List<Konsultacija> getKonsultacijaList() {
        return konsultacijaList;
    }

    public void setKonsultacijaList(List<Konsultacija> konsultacijaList) {
        this.konsultacijaList = konsultacijaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfesora != null ? idProfesora.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesor)) {
            return false;
        }
        Profesor other = (Profesor) object;
        if ((this.idProfesora == null && other.idProfesora != null) || (this.idProfesora != null && !this.idProfesora.equals(other.idProfesora))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Profesor[ idProfesora=" + idProfesora + " ]";
    }
    
}
