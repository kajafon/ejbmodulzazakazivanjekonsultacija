/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Katarina
 */
@Entity
@Table(name = "predmet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Predmet.findAll", query = "SELECT p FROM Predmet p"),
    @NamedQuery(name = "Predmet.findByIdPredmeta", query = "SELECT p FROM Predmet p WHERE p.idPredmeta = :idPredmeta"),
    @NamedQuery(name = "Predmet.findByNaziv", query = "SELECT p FROM Predmet p WHERE p.naziv = :naziv"),
    @NamedQuery(name = "Predmet.findByGodina", query = "SELECT p FROM Predmet p WHERE p.godina = :godina")})
public class Predmet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPredmeta")
    private Integer idPredmeta;
    @Size(max = 50)
    @Column(name = "naziv")
    private String naziv;
    @Column(name = "godina")
    private Integer godina;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPredmeta")
    private List<Profesorpredmet> profesorpredmetList;
    @OneToMany(mappedBy = "predmet")
    private List<Konsultacija> konsultacijaList;

    public Predmet() {
    }

    public Predmet(Integer idPredmeta) {
        this.idPredmeta = idPredmeta;
    }

    public Integer getIdPredmeta() {
        return idPredmeta;
    }

    public void setIdPredmeta(Integer idPredmeta) {
        this.idPredmeta = idPredmeta;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Integer getGodina() {
        return godina;
    }

    public void setGodina(Integer godina) {
        this.godina = godina;
    }

    @XmlTransient
    public List<Profesorpredmet> getProfesorpredmetList() {
        return profesorpredmetList;
    }

    public void setProfesorpredmetList(List<Profesorpredmet> profesorpredmetList) {
        this.profesorpredmetList = profesorpredmetList;
    }

    @XmlTransient
    public List<Konsultacija> getKonsultacijaList() {
        return konsultacijaList;
    }

    public void setKonsultacijaList(List<Konsultacija> konsultacijaList) {
        this.konsultacijaList = konsultacijaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPredmeta != null ? idPredmeta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Predmet)) {
            return false;
        }
        Predmet other = (Predmet) object;
        if ((this.idPredmeta == null && other.idPredmeta != null) || (this.idPredmeta != null && !this.idPredmeta.equals(other.idPredmeta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Predmet[ idPredmeta=" + idPredmeta + " ]";
    }
    
}
