package domen;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-04-19T22:46:04")
@StaticMetamodel(KonsultacijaPK.class)
public class KonsultacijaPK_ { 

    public static volatile SingularAttribute<KonsultacijaPK, Integer> profesor;
    public static volatile SingularAttribute<KonsultacijaPK, Date> vremePocetka;
    public static volatile SingularAttribute<KonsultacijaPK, Date> datum;

}