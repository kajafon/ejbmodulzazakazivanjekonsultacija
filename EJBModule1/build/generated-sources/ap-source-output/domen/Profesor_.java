package domen;

import domen.Konsultacija;
import domen.Profesorpredmet;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-04-19T22:46:04")
@StaticMetamodel(Profesor.class)
public class Profesor_ { 

    public static volatile SingularAttribute<Profesor, Integer> idProfesora;
    public static volatile SingularAttribute<Profesor, String> titula;
    public static volatile ListAttribute<Profesor, Profesorpredmet> profesorpredmetList;
    public static volatile SingularAttribute<Profesor, String> emailProfesora;
    public static volatile SingularAttribute<Profesor, String> prezimeProfesora;
    public static volatile ListAttribute<Profesor, Konsultacija> konsultacijaList;
    public static volatile SingularAttribute<Profesor, String> sifraProfesora;
    public static volatile SingularAttribute<Profesor, String> imeProfesora;

}