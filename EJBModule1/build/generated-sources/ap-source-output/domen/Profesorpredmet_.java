package domen;

import domen.Predmet;
import domen.Profesor;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-04-19T22:46:04")
@StaticMetamodel(Profesorpredmet.class)
public class Profesorpredmet_ { 

    public static volatile SingularAttribute<Profesorpredmet, Profesor> idProfesora;
    public static volatile SingularAttribute<Profesorpredmet, Integer> idVezeProfesorPredmet;
    public static volatile SingularAttribute<Profesorpredmet, Predmet> idPredmeta;

}