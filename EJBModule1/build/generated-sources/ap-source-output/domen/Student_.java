package domen;

import domen.Konsultacija;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-04-19T22:46:04")
@StaticMetamodel(Student.class)
public class Student_ { 

    public static volatile SingularAttribute<Student, String> brojIndeksa;
    public static volatile SingularAttribute<Student, String> imeStudenta;
    public static volatile ListAttribute<Student, Konsultacija> konsultacijaList;
    public static volatile SingularAttribute<Student, String> prezimeStudenta;
    public static volatile SingularAttribute<Student, Integer> idStudent;
    public static volatile SingularAttribute<Student, String> emailStudenta;
    public static volatile SingularAttribute<Student, String> sifraStudenta;

}