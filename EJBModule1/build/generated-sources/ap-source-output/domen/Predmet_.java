package domen;

import domen.Konsultacija;
import domen.Profesorpredmet;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-04-19T22:46:04")
@StaticMetamodel(Predmet.class)
public class Predmet_ { 

    public static volatile SingularAttribute<Predmet, String> naziv;
    public static volatile ListAttribute<Predmet, Profesorpredmet> profesorpredmetList;
    public static volatile ListAttribute<Predmet, Konsultacija> konsultacijaList;
    public static volatile SingularAttribute<Predmet, Integer> idPredmeta;
    public static volatile SingularAttribute<Predmet, Integer> godina;

}