package domen;

import domen.KonsultacijaPK;
import domen.Predmet;
import domen.Profesor;
import domen.Student;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2014-04-19T22:46:04")
@StaticMetamodel(Konsultacija.class)
public class Konsultacija_ { 

    public static volatile SingularAttribute<Konsultacija, String> komentarStudenta;
    public static volatile SingularAttribute<Konsultacija, String> komentarProfesora;
    public static volatile SingularAttribute<Konsultacija, Student> student;
    public static volatile SingularAttribute<Konsultacija, Integer> idKonsultacije;
    public static volatile SingularAttribute<Konsultacija, KonsultacijaPK> konsultacijaPK;
    public static volatile SingularAttribute<Konsultacija, Date> vremeZavrsetka;
    public static volatile SingularAttribute<Konsultacija, Profesor> profesor1;
    public static volatile SingularAttribute<Konsultacija, Predmet> predmet;

}